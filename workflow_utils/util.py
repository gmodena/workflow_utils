from typing import List, Any, Optional

import logging
import importlib
import os
import subprocess
import shutil
import sys

from distlib.database import DistributionPath
from docopt import docopt
from fsspec.registry import register_implementation


class LogHelper():  # pylint: disable=too-few-public-methods
    """
    Inherit from this class (multi-inheritance is fine) to get
    a self.log Logger named after the fully qualified class name
    with 'instance_repr' entry for use by logging formatters.
    instance_repr is populated by calling __repr__, which
    is implemented here to just return the class name.

    Usage:
    class MyClass(LogHelper):

        def my_method(self):
            self.log.info("my message")

    """
    def __init__(self) -> None:
        """
        Creates a class instance specific logger as self.log using __repr__.
        Your implementing class should probably call
        super().__init__() from its __init__ method to get this.
        """
        # Use a logger that is named module.ClassName, but has an extra
        # entry in the logging record instance_repr == self.__repr__().
        self.log = logging.LoggerAdapter(
            logging.getLogger(f'{self.__module__}.{self.__class__.__name__}'),
            {'instance_repr': self.__repr__()}
        )

    def __repr__(self) -> str:
        return self.__class__.__name__


def safe_filename(name: str, maxlen: int = 255) -> str:
    """
    Given a string, returns a 'safe' filename.

    :param name:
    :return: safe filename
    """
    keep_chars = ['.', '_', '-']

    def safe_char(char: str) -> str:
        if char.isalnum() or char in keep_chars:
            return char
        return "_"

    return "".join(safe_char(c) for c in name).rstrip("_")[0:maxlen]


def filter_files_exist(files: List[str]) -> List[str]:
    return [file for file in files if os.path.exists(file)]


def instantiate(class_name: str, **args: Any) -> Any:
    """
    Factory method for instantiating classes from
    kwargs.  class_name is expected to be a fully qualified class name of a
    python class that is importable in PYTHONPATH.

    :param class_name: a fully qualified class name string
    :param **args: These are passed to the class constructor.

    Example:
    ::
        $ tree /path/to/code
        /path/to/code/
        --- ext
            |-- myclass.py
        $ head -n 5 /path/to/code/ext/myclass.py
        class MyClass(object):
            def __init__(name):
                self.name = name
                super().__init__()
        $ export PYTHONPATH=/path/to/code
        ...
        >>> instantiate(type='ext.myclass.MyClass', **{name: 'my_name'})
    """
    if '.' not in class_name:
        raise ValueError(
            f'Cannot import class from {class_name}, it is not in'
            'a fully qualified \'module.ClassName\' format.'
        )
    module_name, symbol_name = class_name.rsplit('.', 1)
    module = importlib.import_module(module_name)
    cls = getattr(module, symbol_name)

    return cls(**args)


def instantiate_all(instances_config: dict) -> dict:
    """
    Given a dict of instance config keyed by name,
    call instantiate() on each of them, and return
    a new dict that keys the name to the instance.

    instances_config must be a dict that looks like:

    instance_name1:
        class_name: my.module.ClassName
        arg1: abc
        arg2: 342
    instance_name2:
        class_name: my.other.ClassName
    # ...

    """
    return {
        name: instantiate(**args)
        for name, args in instances_config.items()
    }


def find_java_home() -> Optional[str]:
    """
    Looks for first JAVA_HOME that exists using JAVA_HOME,
    bigtop-detect-javahome and java -XshowSettings:properties
    """
    java_home = None
    if 'JAVA_HOME' in os.environ:
        java_home = os.environ.get('JAVA_HOME')

    elif os.path.exists('/usr/lib/bigtop-utils/bigtop-detect-javahome'):
        java_home = subprocess.check_output(
            'source /usr/lib/bigtop-utils/bigtop-detect-javahome && echo $JAVA_HOME',
            shell=True,
            executable='/bin/bash'
        ).decode('utf-8').strip()

    elif shutil.which('java'):
        java_home = subprocess.check_output(
            'java -XshowSettings:properties -version 2>&1 > /dev/null '
            '| grep \'java.home\' | awk \'{print $NF}\'',
            shell=True,
        ).decode('utf-8').strip()

    if java_home and os.path.exists(java_home):
        return java_home

    return None


def find_hadoop_home() -> str:
    """
    Looks for first hadoop home that exists in HADOOP_HOME
    and other possible locations,
    """
    possibilities = [
        os.environ.get('HADOOP_HOME', ''),
        os.path.join('/', 'usr', 'lib', 'hadoop'),
    ]
    return next(filter(os.path.exists, possibilities), '')


def find_hadoop_exec() -> Optional[str]:
    """
    Looks for 'hadoop' executable in result of find_hadoop_home()
    or shutil.which('hadoop')
    """
    hadoop: Optional[str] = None
    if os.path.exists(os.path.join(find_hadoop_home(), 'bin', 'hadoop')):
        hadoop = os.path.join(find_hadoop_home(), 'bin', 'hadoop')
    elif shutil.which('hadoop'):
        hadoop = shutil.which('hadoop')
    return hadoop


def get_hadoop_classpath() -> Optional[str]:
    """
    Uses result of find_hadoop_exec() to call
    `hadoop classpath` and returns result.
    """
    hadoop = find_hadoop_exec()
    if hadoop and os.path.exists(hadoop):
        hadoop_classpath = subprocess.check_output(
            [hadoop, 'classpath', '--glob']
        ).decode('utf-8').strip()
    else:
        hadoop_classpath = None

    return hadoop_classpath


def set_hadoop_env_vars(force: bool = False) -> None:
    """
    Sets pyarrow related Hadoop environment variables if they aren't already set.
    This automates pyarrow (and fsspec) using the newer pyarrow HDFS API without
    forcing the user to manually set these.
    See: https://arrow.apache.org/docs/python/filesystems.html#hadoop-distributed-file-system-hdfs

    Env vars set:
    - JAVA_HOME
    - HADOOP_HOME
    - CLASSPATH

    :param force:
        If true, env vars will be changed even if they are already set.
    """

    env_var_to_func = [
        ('JAVA_HOME', find_java_home),
        ('HADOOP_HOME', find_hadoop_home),
        ('CLASSPATH', get_hadoop_classpath),
    ]

    for env_var, func in env_var_to_func:
        if force or env_var not in os.environ:
            value = func()
            if value:
                os.environ[env_var] = value


def fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars: bool = True) -> None:
    """
    Registers hdfs:// filesystems to always use the arrow_hdfs://
    implementation.  This uses the newer pyarrow HDFS API.
    Call this to force fsspec to always use the new pyarrow API
    instead of the old one for all hdfs:// URLs, instead of
    manually making those URLs use arrow_hdfs:// APIs.

    NOTE: This should be removed after
    https://github.com/fsspec/filesystem_spec/issues/874 is resolved.
    """
    register_implementation(
        name='hdfs',
        cls='fsspec.implementations.arrow.HadoopFileSystem',
        clobber=True,
        errtxt='pyarrow and local java libraries required for HDFS',
    )
    if should_set_hadoop_env_vars:
        set_hadoop_env_vars()


def sys_path(python_exec: Optional[str] = None) -> List[str]:
    """
    Looks a sys.path for either the current running python, or
    an external python intepreter.  If python_exec is set, its
    sys.path will be obtained by running a print(sys.path) in a
    subprocess.

    :param python_exec:
        Path to python interpreter executable to get sys.path for.

    """
    if not python_exec:
        return sys.path

    if not os.access(python_exec, os.X_OK):
        raise ValueError(
            'python_exec must be a path to an executable python interpreter, '
            f'given {python_exec}'
        )

    # shell out to python_exec and print its sys.path
    sys_path_str = subprocess.check_output(
        [python_exec, '-c', 'import sys; print(sys.path)']
    ).decode('utf-8').strip()

    # sys_path_str should eval as a python list.
    return list(eval(sys_path_str))  # pylint: disable=eval-used


def package_version(package_name: str, python_exec: Optional[str] = None) -> str:
    """
    Uses distlib to look up the version of an installed package.
    If python_exec is given, the distribution paths in which to search
    for package_name will be be determined using sys_path to
    ask the python_exec for its sys.path.

    This is useful for CI/CD when using e.g. conda-dist
    to generate artifacts of conda environments.

    :param package_name:
        The name of the python package to get the version of.

    :param python_exec:
        Path to python interpreter executable to use when looking up
        installed package version.
    """
    # Get the sys.path to look for packages in.
    # If python_exec is None, this will just use the current sys.path directly.
    python_sys_path = sys_path(python_exec)
    dist_path = DistributionPath(python_sys_path)
    distribution = dist_path.get_distribution(package_name)

    if distribution is None:
        raise ValueError(
            f'Package {package_name} is not installed, '
            f'cannot discover package version. (python_exec: {python_exec})'
        )

    return str(distribution.version)


def package_version_cli() -> None:
    """
    CLI function to call package_version and print result.
    """

    script_name = os.path.basename(sys.argv[0])
    doc = f"""
Prints the python package version of a package importable
by the given python interpreter executlable.

Usage:
  {script_name} [--python-exec=<python_exec>] <package_name>

Options:

  --python-exec=<python_exec>
    Path to python interpreter

    """
    args = docopt(doc, sys.argv[1:])
    print(package_version(args['<package_name>'], args['--python-exec']))


# NOTE: i dunno about this.
def setup_logging(level: Optional[str] = None) -> None:
    """
    Configures basic logging defaults.
    If level is not given, but the environment variable LOG_LEVEL
    is set, it will be used as the level.  Otherwise INFO is the default level.

    An instance_repr entry will be added to every log record if one
    doesn't already exist.  This allows us to use formatters
    that refer to %(instance_repr).  LogHelper instances
    always add an instance_repr entry to the log record.

    Args:
        level (str): log level
    """
    if level is None:
        level = getattr(
            logging, os.environ.get('LOG_LEVEL', 'INFO')
        )

    logging.basicConfig(
        level=level,
        format='%(asctime)s %(levelname)-8s %(instance_repr)-40s %(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S%z',
    )

    logging.getLogger('conda').setLevel(logging.INFO)

    # Since we use instance_repr in the format of the basic logger,
    # All log records also need to have an instance_repr entry.
    # Add a logging filter to the root logger's handlers to
    # make sure that instance_repr is set if a child logger hasn't set it.
    def inject_instance_name(record: Any) -> bool:
        if not hasattr(record, 'instance_repr'):
            record.instance_repr = record.name
        return True

    root_logger = logging.getLogger()
    for handler in root_logger.handlers:
        handler.addFilter(inject_instance_name)
