#!/bin/bash

# Downloads and installs a miniconda base environment

# Python version to use in conda environment and for Airflow dependency constraints.
: ${PYTHON_VERSION:="3.7"}

# Version of miniconda installer to download.
: ${MINICONDA_VERSION:="4.10.3"}

# Arch of miniconda installer to download.
# Default is to infer using uname.
if [ -z "${MINICONDA_ARCH}" ]; then
    kernel_name=$(uname -s)
    arch=$(uname -m)

    # conda uses 'MacOSX' in downloader URLs.
    if [ "${kernel_name}" == "Darwin" ]; then
        kernel_name="MacOSX"
    fi

MINICONDA_ARCH="${kernel_name}-${arch}"
fi

# Path in which to install the base miniconda env.
: ${MINICONDA_ENV_PREFIX:="/srv/miniconda"}

# If set to true, a previously downloaded installer will be removed
# and redownloaded.
: ${MINICONDA_INSTALLER_CLEAN:="false"}

PYTHON_MAJOR_MINOR_VERSION=$(echo "${PYTHON_VERSION}" | sed 's@\.@@g')

MINICONDA_INSTALLER_NAME=Miniconda3-py${PYTHON_MAJOR_MINOR_VERSION}_${MINICONDA_VERSION}-${MINICONDA_ARCH}.sh
MINICONDA_INSTALLER_URL="https://repo.anaconda.com/miniconda/${MINICONDA_INSTALLER_NAME}"

MINICONDA_INSTALLER_FILE="/tmp/${MINICONDA_INSTALLER_NAME}"
MINICONDA_INSTALLER_FILE_HASH_FILE="/tmp/${MINICONDA_INSTALLER_NAME}.sha256sum"

# After download $MINICONDA_INSTALLER_FILE sha256sum will be verified with the hashes listed at this URL.
MINICONDA_HASHES_URL="https://raw.githubusercontent.com/conda/conda-docs/master/docs/source/miniconda_hashes.rst"

set -x
set -e

# Verifies the dowloaded miniconda installer SHA256 hash
#
function verify_miniconda_installer_checksum {
    if [ "${MINICONDA_INSTALLER_CLEAN}" == "true" ]; then
        rm -fv "${MINICONDA_INSTALLER_FILE_HASH_FILE}"
    fi

    test -e "${MINICONDA_INSTALLER_FILE_HASH_FILE}" ||
        curl -s "${MINICONDA_HASHES_URL}" | \
            grep "${MINICONDA_INSTALLER_NAME}" | \
            awk '{print $NF}' | \
            tr -d '`' > \
            "${MINICONDA_INSTALLER_FILE_HASH_FILE}"

    miniconda_installer_hash_expected=$(cat ${MINICONDA_INSTALLER_FILE_HASH_FILE}) && \
        miniconda_installer_hash=$(shasum -a 256 "${MINICONDA_INSTALLER_FILE}" | awk '{print $1}') && \
        test "${miniconda_installer_hash}" == "${miniconda_installer_hash_expected}"
    retval=$?

    if [ ${retval} -eq 0 ]; then
        echo "${MINICONDA_INSTALLER_FILE} download succeeded sha256 checksum verification: ${miniconda_installer_hash}"
    else
        echo "${MINICONDA_INSTALLER_FILE} download failed sha256 checksum verification."
    fi

    return $retval
}


# Downloads the miniconda installer.
#
function download_miniconda_installer {
    if [ "${MINICONDA_INSTALLER_CLEAN}" == "true" ]; then
        rm -fv "${MINICONDA_INSTALLER_FILE}"
    fi

    test -e "${MINICONDA_INSTALLER_FILE}" || curl "${MINICONDA_INSTALLER_URL}" > "${MINICONDA_INSTALLER_FILE}" && \
        verify_miniconda_installer_checksum
}

# Creates a new miniconda env using the downloaded miniconda installer.
#
function install_miniconda_env {
    download_miniconda_installer && \
    rm -rfv "${MINICONDA_ENV_PREFIX}" && \
        bash "${MINICONDA_INSTALLER_FILE}" -b -p "${MINICONDA_ENV_PREFIX}"
}

install_miniconda_env
