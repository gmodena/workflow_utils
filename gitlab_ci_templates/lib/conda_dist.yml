# Variables and a script snippet that will build and publish a conda dist env.

include:
  - local: gitlab_ci_templates/lib/conda.yml
  - local: gitlab_ci_templates/lib/workflow_utils.yml

variables:
  # TODO: Consider defaulting to a common gitlab project for artifacts, e.g. data-engineering/artifacts ?
  GENERIC_PACKAGE_REGISTRY_URI:
    description: >
      URI of Gitlab Generic Package Registry to which the conda dist env artifact should be uploaded.
      Defaults to the Generic Package Registry of the current Gitlab project.
    value: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic

  # Override this if your python project name is different than your gitlab repo name.
  PACKAGE_NAME:
    description: >
      Name of your python package that you will be building a conda dist env for.
      Defaults to CI_PROJECT_NAME.
    value: ${CI_PROJECT_NAME}

  PACKAGE_VERSION:
    description: >
      Version of your project. If not set, instead, PACKAGE_VERSION_SCRIPT will be run
      to get the project version.
    value: ''

  PACKAGE_VERSION_SCRIPT:
    description: >
      Script to run to get PACKAGE_VERSION if it is not set.
      Defaults to using workflow_utils' package-version CLI
      to shell out to the built conda dist env python and get the installed
      version of PACKAGE_NAME.
    value: 'package-version --python-exec=./dist/conda_dist_env/bin/python ${PACKAGE_NAME}'


# script snipped that runs conda-dist to build a conda_dist_env.tgz file for the project.
# The conda dist env will be published to $PACKAGE_REGISTRY_URI.
.conda_dist_publish_script:
    - !reference [.conda_setup_script]
    # Install data-engineering/workflow-utils to get conda-dist
    - !reference [.workflow_utils_install_script]
    - apt install -y curl ca-certificates  
    # Use conda-dist to build a packed conda environment with this project and its dependencies.
    - conda-dist
    # Run PACKAGE_VERSION_SCRIPT from conda dist env's python after conda-dist has installed
    # the project into it. This allows the script to use importlib.metadata to get the version
    # number of the package installed in the conda dist env.
    - 'test -z "${PACKAGE_VERSION}" && PACKAGE_VERSION=$(${PACKAGE_VERSION_SCRIPT})'
    - 'echo "Publishing conda dist env to ${PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/$PACKAGE_NAME-$PACKAGE_VERSION.conda.tgz"'
    - 'curl -v --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "./dist/conda_dist_env.tgz" "${GENERIC_PACKAGE_REGISTRY_URI}/${PACKAGE_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}-${PACKAGE_VERSION}.conda.tgz"'
