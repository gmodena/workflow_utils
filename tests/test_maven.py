import pytest
from workflow_utils.artifact.maven import (
    parse_maven_coordinate,
    maven_artifact_dir,
    maven_artifact_uri,
)


@pytest.mark.parametrize(['coordinate', 'expected'], [
    (
        'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        {
            'group_id': 'org.apache.hadoop',
            'artifact_id': 'hadoop-yarn-client',
            'version': '2.10.1',
            'packaging': 'jar',
            'classifier': None,
        },
    ),

    (
        'python:alembic:whl:1.2.1-py2.py3-none-any',
        {
            'group_id': 'python',
            'artifact_id': 'alembic',
            'version': '1.2.1-py2.py3-none-any',
            'packaging': 'whl',
            'classifier': None,
        },
    ),

    (
        'org.my.thing:my-thing:my-packaging:my-classifier:1.0.0',
        {
            'group_id': 'org.my.thing',
            'artifact_id': 'my-thing',
            'version': '1.0.0',
            'packaging': 'my-packaging',
            'classifier': 'my-classifier',
        },
    ),
])
def test_parse_maven_coordinate(coordinate, expected):
    assert parse_maven_coordinate(coordinate) == expected


@pytest.mark.parametrize(['coordinate', 'expected'], [
    (
        'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        'org/apache/hadoop/hadoop-yarn-client/2.10.1'
    ),

    (
        'python:alembic:whl:1.2.1-py2.py3-none-any',
        'python/alembic/1.2.1-py2.py3-none-any'
    ),

    (
        'org.my.thing:my-thing:my-packaging:my-classifier:1.0.0',
        'org/my/thing/my-thing/1.0.0'
    ),
])
def test_maven_artifact_dir(coordinate, expected):
    assert maven_artifact_dir(coordinate) == expected


@pytest.mark.parametrize(['coordinate', 'expected'], [
    (
        'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        'org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'
    ),

    (
        'python:alembic:whl:1.2.1-py2.py3-none-any',
        'python/alembic/1.2.1-py2.py3-none-any/alembic-1.2.1-py2.py3-none-any.whl'
    ),

    (
        'org.my.thing:my-thing:my-packaging:my-classifier:1.0.0',
        'org/my/thing/my-thing/1.0.0/my-thing-1.0.0-my-classifier.my-packaging'
    ),
])
def test_maven_artifact_uri(coordinate, expected):
    assert maven_artifact_uri(coordinate) == expected


@pytest.mark.parametrize(['coordinate', 'repo_uri', 'expected'], [
    (
        'org.apache.hadoop:hadoop-yarn-client:2.10.1',
        'http://my-maven-repo.org/artifacts',
        'http://my-maven-repo.org/artifacts/org/apache/hadoop/hadoop-yarn-client/2.10.1/hadoop-yarn-client-2.10.1.jar'  # pylint: disable=line-too-long # noqa: E501

    ),

    (
        'python:alembic:whl:1.2.1-py2.py3-none-any',
        'http://my-maven-repo.org/artifacts',
        'http://my-maven-repo.org/artifacts/python/alembic/1.2.1-py2.py3-none-any/alembic-1.2.1-py2.py3-none-any.whl'  # pylint: disable=line-too-long # noqa: E501
    ),

    (
        'org.my.thing:my-thing:my-packaging:my-classifier:1.0.0',
        'http://my-maven-repo.org/artifacts',
        'http://my-maven-repo.org/artifacts/org/my/thing/my-thing/1.0.0/my-thing-1.0.0-my-classifier.my-packaging'  # pylint: disable=line-too-long # noqa: E501
    ),
])
def test_maven_artifact_uri_with_prefix(coordinate, repo_uri, expected):
    assert maven_artifact_uri(coordinate, repo_uri=repo_uri) == expected


def test_invalid_maven_coordinate():
    with pytest.raises(ValueError):
        assert parse_maven_coordinate('bad bad')
